﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace GameEngine.Interfaces
{
    /// <summary>
    /// Properties required for a game object to be drawable
    /// </summary>
    public interface IGameDrawable
    {
        float X { get; }
        float Y { get; }
        float Width { get; }
        float Height { get; }

        float Rotation { get; }
        
        /// <summary>
        /// Specify whether the Bounding Box should be drawn
        /// </summary>
        bool BoundingBoxVisible { get; }

        /// <summary>
        /// Value Specifying if the Drawable Object should currently be visible
        /// </summary>
        bool Visible { get; }

        /// <summary>
        /// Color mask with which to draw the SpriteBatch. Should be White by Default
        /// </summary>
        Color DrawColor { get; }

        /// <summary>
        /// Relative Origin. 0,0 Being Top Left Corner and 1,1 Being Bottom Right Corner/+
        /// </summary>

        Vector2 Origin { get; }

        Texture2D GetTexture(GameTime GameTime);
        Rectangle GetSourceRectangle(GameTime GameTime);
    }
}