﻿using GameEngine.GameObjects;

namespace GameEngine.Interfaces
{
    public interface IWorldGenerator
    {
        Map Generate(int Width, int Height);
    }
}