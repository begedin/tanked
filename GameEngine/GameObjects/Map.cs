﻿﻿using GameEngine.Interfaces;

namespace GameEngine.GameObjects
{
    /// <summary>
    /// represents a game map that be used. Contains tile information as well as 
    /// the type of ground pallette used for this particular map
    /// </summary>
    public class Map
    {
        public int Width { get { return _mapTiles.Length; } }
        public int Height { get { return _mapTiles[0].Length; } }
        public IGroundPallette GroundPallette { get; private set; }

        /// <summary>
        /// provides access to Tile information through indexed properties
        /// </summary>

        public byte this[int X, int Y]
        {
            get { return _mapTiles[X][Y]; }
            set { _mapTiles[X][Y] = value; }
        }

        private byte[][] _mapTiles = null;

        public Map(int Width, int Height, IGroundPallette GroundPallette)
        {
            this._mapTiles = new byte[Width][];
            this.GroundPallette = GroundPallette;

            for (int i = 0; i < Width; i++)
                this._mapTiles[i] = new byte[Height];
        }
    }
}