﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tanked.Timing
{
    /// <summary>
    /// <see cref="DelayTimer"/> expired delegate
    /// </summary>
    /// <param name="timer"></param>
    public delegate void DelayExpiredHandler(DelayTimer timer);

    /// <summary>
    /// <see cref="Timer"/> expired delegate
    /// </summary>
    /// <param name="timer"></param>
    public delegate void TimerExpiredHandler(Timer timer);

    /// <summary>
    /// Main TimerManager singleton class. Call Instance.Update from the main update.
    /// </summary>
    public sealed class TimerManager
    {
        private readonly List<DelayTimer> _delayTimerList = new List<DelayTimer>();
        private readonly List<Timer> _timerList = new List<Timer>();

        private readonly List<DelayTimer> _delayTimerRemoveList = new List<DelayTimer>();
        private readonly List<Timer> _timerRemoveList = new List<Timer>();

        internal List<DelayTimer> DelayTimerList
        {
            get { return _delayTimerList; }
        }

        
        internal List<Timer> TimerList
        {
            get { return _timerList; }
        }

        private TimerManager() 
        {
        }

        static readonly TimerManager _instance = new TimerManager();
        public static TimerManager Instance
        {
            get {return _instance;}
        }

        public void Update(GameTime time)
        {
            foreach (var timer in _timerList)
                timer.Update(time);

            foreach (var timer in _delayTimerList)
                timer.Update(time);

            foreach (var timer in _timerRemoveList)
                _timerList.Remove(timer);

            foreach (var timer in _delayTimerRemoveList)
                _delayTimerList.Remove(timer);

            _timerRemoveList.Clear();
            _delayTimerRemoveList.Clear();
                
        }

        internal void Add(Timer timer)
        {
            TimerList.Add(timer);
        }

        internal void Remove(Timer timer)
        {
            _timerRemoveList.Add(timer);
        }

        public void Add(DelayTimer timer)
        {
            _delayTimerList.Add(timer);
        }

        public void Remove(DelayTimer timer)
        {
            _delayTimerRemoveList.Add(timer);
        }
    }
}
