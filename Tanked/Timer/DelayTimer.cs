﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tanked.Timing
{
    /// <summary>
    /// Timer which starts at a delay
    /// </summary>
    public class DelayTimer
    {
        /// <summary>
        /// Timer has expired event
        /// </summary>
        public event DelayExpiredHandler ExpiredEvent;

        private readonly Timer _delayTimer= new Timer();
        private readonly Timer _runTimer = new Timer();

        public string Name { get; set; }

        /// <summary>
        /// Seconds until the timer starts.
        /// </summary>
        public float SecondsUntilStart
        {
            set { _delayTimer.SecondsUntilExpire = (int)(value); }
            get { return _delayTimer.SecondsUntilExpire; }
        }

        /// <summary>
        /// Miliseconds until the timer starts.
        /// </summary>
        public float MilisecondsUntilStart
        {
            set { _delayTimer.MilisecondsUntilExpire = (int)(value); }
            get { return _delayTimer.MilisecondsUntilExpire; }
        }

        /// <summary>
        /// Seconds until the timer expires.
        /// </summary>
        public float SecondsUntilExpire
        {
            set { _runTimer.SecondsUntilExpire = (int)(value); }
            get { return _runTimer.SecondsUntilExpire; }
        }

        /// <summary>
        /// Miliseconds until the timer expires.
        /// </summary>
        public float MilisecondsUntilExpire
        {
            set { _runTimer.MilisecondsUntilExpire = (int)(value); }
            get { return _runTimer.MilisecondsUntilExpire; }
        }


        public DelayTimer(string name)
        {
            this.Name = name;
        }

        public DelayTimer()
        {
            this.Name = "Unnamed";
            Reset();
        }

        public DelayTimer(string name, float delayMiliseconds, float durationMiliseconds)
        {
            throw new NotImplementedException();
        }

        public bool IsExpired
        {
            get { return _runTimer.IsExpired; }
        }

        public bool IsStarted
        {
            get { return _delayTimer.IsExpired; }
        }

        public void Reset()
        {
            _delayTimer.Reset();
            _runTimer.Reset();
        }

        public void Update(GameTime time)
        {
            if (_delayTimer.IsExpired && _runTimer.IsRunning && !_runTimer.IsExpired)
            {
                _runTimer.Start();
            }

            if (_runTimer.IsExpired)
            {
                if (ExpiredEvent != null) ExpiredEvent(this);
                TimerManager.Instance.Remove(this);
            }
        }

        /// <summary>
        /// Delta from 0 to 1 depending on how far the timer has advanced
        /// </summary>
        public float Delta
        {
            get
            {
                if (_runTimer.IsRunning)
                    return _runTimer.Delta;

                if (!IsStarted)
                    return 0.0f;
                if (IsExpired)
                    return 0.1f;

                return 0.0f;
            }
        }

        /// <summary>
        /// Interpolates (smoothly) two float timer values.
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public float FlatSmoothStep(float start, float end)
        {
            float fDelta = Delta;
            return MathHelper.SmoothStep(start, end, fDelta);
        }

        /// <summary>
        /// Interpolates (smoothly) two Vector2 timer values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public Vector2 Vector2SmoothStep(Vector2 start, Vector2 end)
        {
            float fDelta = Delta;
            return Vector2.SmoothStep(start, end, fDelta);
        }

        /// <summary>
        /// Interpolates (smoothly) two Vector3 timer values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public Vector3 Vector3SmoothStep(Vector3 start, Vector3 end)
        {
            float fDelta = Delta;
            return Vector3.SmoothStep(start, end, fDelta);
        }

        /// <summary>
        /// Performas linear interpolation of two vectors
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public Vector2 Vector2Lerp(Vector2 start, Vector2 end)
        {
            float fDelta = Delta;
            return Vector2.Lerp(start, end, fDelta);
        }

        /// <summary>
        /// Performs linear interpolation of two Vector3 values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated Vector3 value.</returns>
        public Vector3 Vector3Lerp(Vector3 start, Vector3 end)
        {
            float fDelta = Delta;
            return Vector3.Lerp(start, end, fDelta);
        }

        /// <summary>
        /// Performs linear interpolation of two Vector4 values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated Vector4 value.</returns>
        public Vector4 Vector4Lerp(Vector4 start, Vector4 end)
        {
            float fDelta = Delta;
            return Vector4.Lerp(start, end, fDelta);
        }
    }
}
