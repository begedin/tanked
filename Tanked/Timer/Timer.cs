﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tanked.Timing
{
    /// <summary>
    /// A basic timer, for timing in seconds/miliseconds
    /// </summary>
    public class Timer
    {
        /// <summary>
        /// Timer expired event
        /// </summary>
        public TimerExpiredHandler ExpiredEvent;

        private float _timeExpired;
        private float _timeTarget;

        public string Name { get; set; }

        /// <summary>
        /// Seconds until the timer expires.
        /// </summary>
        public float SecondsUntilExpire
        {
            set { _timeTarget = (int)(value * 1000); }
            get { return _timeTarget / 1000.0f; }
        }

        /// <summary>
        /// Miliseconds until the timer expires.
        /// </summary>
        public float MilisecondsUntilExpire
        {
            set { _timeTarget = (int)(value); }
            get { return _timeTarget; }
        }

        public Timer(string name)
        {
            this.Name = name;
        }

        public Timer()
        {
            Name = "Unnamed";
            Reset();
        }

        public Timer(string name, float miliseconds)
        {
            this.Name = name;
            this.MilisecondsUntilExpire = miliseconds;
        }

        public bool IsRunning { get; set; }

        public bool IsExpired { get; set; }

        ~Timer()
        {
            TimerManager.Instance.TimerList.Remove(this);
        }

        /// <summary>
        /// Resets the timer
        /// </summary>
        public void Reset()
        {
            IsExpired = false;
            IsRunning = false;
            _timeExpired = 0;
        }

        /// <summary>
        /// Returns delta between 0 and 1, signifying how far through time we are
        /// </summary>
        public float Delta
        {
            get
            {
                if (_timeTarget != -1)
                {
                    return _timeExpired / _timeTarget;
                }

                return 0.0f;
            }
        }

        /// <summary>
        /// Performs a smooth (Hermite) interpolation of two vectors.
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public Vector2 Vector2SmoothStep(Vector2 start, Vector2 end)
        {
            float fDelta = Delta;

            return Vector2.SmoothStep(start, end, fDelta);
        }

        /// <summary>
        /// Performas linear interpolation of two vectors
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated value</returns>
        public Vector2 Vector2Lerp(Vector2 start, Vector2 end)
        {
            float fDelta = Delta;
            return Vector2.Lerp(start, end, fDelta);
        }

        /// <summary>
        /// Performs linear interpolation of two Vector3 values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated Vector3 value.</returns>
        public Vector3 Vector3Lerp(Vector3 start, Vector3 end)
        {
            float fDelta = Delta;
            return Vector3.Lerp(start, end, fDelta);
        }

        /// <summary>
        /// Performs linear interpolation of two Vector4 values
        /// </summary>
        /// <param name="start">Value when timer starts.</param>
        /// <param name="end">Value when timer ends.</param>
        /// <returns>Interpolated Vector4 value.</returns>
        public Vector4 Vector4Lerp(Vector4 start, Vector4 end)
        {
            float fDelta = Delta;
            return Vector4.Lerp(start, end, fDelta);
        }

        /// <summary>
        /// Starts the timer.
        /// </summary>
        public void Start()
        {
            if (!IsRunning && !TimerManager.Instance.TimerList.Contains(this))
                TimerManager.Instance.Add(this);

            IsExpired = false;
            _timeExpired = 0;
            IsRunning = true;
        }

        /// <summary>
        /// Stops the timer.
        /// </summary>
        public void Stop()
        {
            IsRunning = false;
            TimerManager.Instance.Remove(this);
        }

        /// <summary>
        /// Updates the timer and changes it to expired if the time runs out
        /// </summary>
        /// <param name="time"></param>
        public void Update(GameTime time)
        {
            if (!IsRunning) return;
            _timeExpired += time.ElapsedGameTime.Milliseconds;

            if (_timeExpired >= _timeTarget && _timeTarget != -1)
            {
                IsExpired = true;
                if (ExpiredEvent != null) ExpiredEvent(this);
                Stop();
            }
        }
    }
}
