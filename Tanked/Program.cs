﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace Tanked
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static TankedGame game;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            game = new TankedGame();
            game.Run();
        }
    }
}
