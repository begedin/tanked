﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.Interfaces;
using Tanked.Timing;

namespace Tanked.Projectiles
{
    class Bullet : Projectile
    {
        protected override float timePerTile { get { return 200; } }

        public Bullet(Owner owner, Direction direction, Texture2D texture, Vector2 position)
            : base(owner, direction, texture, new Point(24, 24), Vector4.Zero, new Point(0, 0), new Point(3, 1), position, Constants.DefaultScale)
        {
            this.moveTimer = new Timer("projectileMotionTimer", this.timePerTile);
            this.moveTimer.Start();
        }
    }
}
