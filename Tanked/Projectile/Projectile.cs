﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.Interfaces;
using Tanked.Timing;

namespace Tanked.Projectiles
{
    /// <summary>
    /// Base object for projectiles. Inherits Sprite, Spawns at location and moves.
    /// 
    /// CREDIT: https://github.com/crneville/XNA/blob/master/1061fight/1061fight
    /// </summary>
    public abstract class Projectile : AnimatedSprite
    {
        protected float baseTimePerTile = 50;
        protected int baseDamage = 0;

        public virtual int DamageDone
        {
            get { return baseDamage; }
            set { baseDamage = value; }
        }

        #region Identification
        static int ProjectileUID = 0;

        protected int projectileUID = 0;

        static public int GetProjectileUID
        {
            get { return Projectile.ProjectileUID++; }
        }

        public int ProjectileIdentifier
        {
            get { return projectileUID; }
        }

        protected Owner owner;

        #endregion

        #region Motion

        protected Vector2 lastPosition;
        protected Vector2 nextPosition;

        protected Timer moveTimer;

        protected virtual float timePerTile { get { return 50; } }

        protected Direction direction;

        #endregion

        /// <summary>
        /// Projectile with damage, animation,  starting position, velocity, scale and collision offset
        /// </summary>
        /// <param name="damageOnImpact">The amount of damage the projectile does</param>
        /// <param name="spriteTexture">The texture file for the projectile</param>
        /// <param name="spriteFrameSize">The size of a single frame</param>
        /// <param name="spriteCollisionBoxOffset">The offset for the collision</param>
        /// <param name="spriteStartingFrame">First frame to draw</param>
        /// <param name="spriteSheetDimensions">Dimensions of the entire spritesheet</param>
        /// <param name="startingPosition">Initial position of the projectile, origin</param>
        /// <param name="velocity">Initial velocity of the projectile</param>
        /// <param name="spriteScale">Initial scale of the projectile</param>
        public Projectile(Owner owner, Direction direction, Texture2D spriteTexture, 
                          Point spriteFrameSize, Vector4 spriteCollisionBoxOffset, 
                          Point spriteStartingFrame, Point spriteSheetDimensions, 
                          Vector2 startingPosition, float spriteScale)
            : base(spriteTexture, spriteFrameSize, spriteCollisionBoxOffset, 
            spriteStartingFrame, spriteSheetDimensions, startingPosition, spriteScale)
        {
            this.direction = direction;
            this.owner = owner;

            this.moveTimer = new Timer("projectileMotionTimer", this.timePerTile);
            this.moveTimer.Start();

            this.lastPosition = new Vector2(startingPosition.X, startingPosition.Y);
            this.currentPosition = new Vector2(startingPosition.X, startingPosition.Y);
            this.nextPosition = getNextPosition();
        }

        /// <summary>
        /// Updates the position of the projectile, based on velocity 
        /// and then calls the base method to update the current frame
        /// </summary>
        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            if (moveTimer.IsRunning)
                currentPosition = moveTimer.Vector2Lerp(lastPosition, nextPosition);

            if (moveTimer.IsExpired)
            {
                moveTimer.Reset();
                moveTimer.Start();
                lastPosition = new Vector2(currentPosition.X, currentPosition.Y);
                nextPosition = getNextPosition();
            }

            base.Update(gameTime, clientBounds);
        }

        private Vector2 getNextPosition()
        {
            Vector2 newPosition = new Vector2();

            if (this.direction == Direction.Up)
                newPosition = new Vector2(this.currentPosition.X, this.currentPosition.Y - Constants.TileSize);
            if (this.direction == Direction.Down)
                newPosition = new Vector2(this.currentPosition.X, this.currentPosition.Y + Constants.TileSize);
            if (this.direction == Direction.Left)
                newPosition = new Vector2(this.currentPosition.X - Constants.TileSize, this.currentPosition.Y);
            if (this.direction == Direction.Right)
                newPosition = new Vector2(this.currentPosition.X + Constants.TileSize, this.currentPosition.Y);

            return newPosition;
        }
    }
}
