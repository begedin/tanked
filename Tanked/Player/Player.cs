﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Tanked.Components;
using Tanked.Projectiles;
using Tanked.Timing;

namespace Tanked.Players
{
    class Player : AnimatedSprite
    {
        enum PlayerState
        {
            Still,
            Moving,
        }

        PlayerState playerState;

        Texture2D projectileTexture;

        Direction direction;
        Timer moveTimer;

        Vector2 lastPosition;
        Vector2 nextPosition;

        public Player(Texture2D playerTexture, Texture2D projectileTexture)
            :base (playerTexture, new Point(24,24), Vector4.Zero, new Point(0,0), new Point(3,1), Vector2.Zero, 0.0f, Constants.DefaultScale)
        {
            lastPosition = new Vector2(300, 300);
            nextPosition = new Vector2(300, 300);
            currentPosition = new Vector2(300, 300);
            playerState = PlayerState.Still;
            direction = Direction.Up;

            this.projectileTexture = projectileTexture;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            base.Draw(spriteBatch);
        }

        public override void Update(GameTime gameTime, Rectangle clientBounds)
        {
            if (playerState == PlayerState.Moving)
            {
                currentPosition = moveTimer.Vector2Lerp(lastPosition, nextPosition);
                base.Update(gameTime, clientBounds);
            }
        }

        private void DoneMoving(Timer timer)
        {
            TimerManager.Instance.Remove(timer);
            playerState = PlayerState.Still;
            lastPosition = new Vector2(currentPosition.X, currentPosition.Y);
        }

        internal void HandleInput(ScreenManagement.InputState input)
        {
            KeyboardState currentState = input.CurrentKeyboardStates[0];
            if (playerState == PlayerState.Still)
            {

                if (currentState.IsKeyDown(Keys.Up) || currentState.IsKeyDown(Keys.Down) ||
                    currentState.IsKeyDown(Keys.Left) || currentState.IsKeyDown(Keys.Right))
                {
                    if (currentState.IsKeyDown(Keys.Down))
                    {
                        nextPosition = new Vector2(lastPosition.X, lastPosition.Y + Constants.TileSize);
                        angle = MathHelper.Pi;
                        direction = Direction.Down;
                    }

                    if (currentState.IsKeyDown(Keys.Up))
                    {
                        nextPosition = new Vector2(lastPosition.X, lastPosition.Y - Constants.TileSize);
                        angle = 0;
                        direction = Direction.Up;
                    }

                    if (currentState.IsKeyDown(Keys.Left))
                    {
                        nextPosition = new Vector2(lastPosition.X - Constants.TileSize, lastPosition.Y);
                        angle = -MathHelper.PiOver2;
                        direction = Direction.Left;
                    }

                    if (currentState.IsKeyDown(Keys.Right))
                    {
                        nextPosition = new Vector2(lastPosition.X + Constants.TileSize, lastPosition.Y);
                        angle = MathHelper.PiOver2;
                        direction = Direction.Right;
                    }

                    playerState = PlayerState.Moving;
                    moveTimer = new Timer("playerMotion", 900);
                    moveTimer.Start();
                    moveTimer.ExpiredEvent += new TimerExpiredHandler(DoneMoving);
                    TimerManager.Instance.Add(moveTimer);
                    AudioManager.PlaySoundEffect("Roll");
                }
            }

            if (currentState.IsKeyDown(Keys.Space))
            {
                Bullet bullet = new Bullet(Owner.Player, this.direction, projectileTexture, this.Position);
                ProjectileManager.Add(bullet);
            }
        }
    }
}
