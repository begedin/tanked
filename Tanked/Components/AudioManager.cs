﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Tanked.Components
{
    public class AudioManager : GameComponent
    {
        #region Singleton
        /// <summary>
        /// the singleton
        /// </summary>
        private static AudioManager audioManager = null;
        #endregion

        /// <summary>
        /// sound effect library
        /// </summary>
        private static Dictionary<string, SoundEffect> soundEffects;


        /// <summary>
        /// content loads here instead of LoadContent, since it's a singleton
        /// </summary>
        /// <param name="game"></param>
        private AudioManager(Game game)
            : base(game)
        {
            try
            {
                soundEffects = new Dictionary<string, SoundEffect>();
                soundEffects.Add("Roll", Game.Content.Load<SoundEffect>(@"Sounds/Roll"));
            }
            catch (NoAudioHardwareException)
            {
                //do nothing, the game will be silent
            }
        }

        public static void Initialize(Game game)
        {
            if (game == null)
                return;

            audioManager = new AudioManager(game);
            game.Components.Add(audioManager);
        }

        /// <summary>
        /// Plays a fire-and-forget sound
        /// </summary>
        /// <param name="soundName">The name of the sound to be played</param>
        public static void PlaySoundEffect(string soundName)
        {
            if (audioManager == null || soundEffects == null)
                return;

            if (soundEffects.ContainsKey(soundName))
                soundEffects[soundName].Play();
        }
    }
}
