﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.Projectiles;

namespace Tanked.Components
{
    public sealed class ProjectileManager : DrawableGameComponent
    {
        private static ProjectileManager bulletManager = null;

        private static List<Projectile> projectiles;
        private static List<Projectile> projectilesToRemove;

        SpriteBatch spriteBatch;

        private static Texture2D texture;

        private ProjectileManager(Game game)
            : base(game)
        {
            projectiles = new List<Projectile>();
            projectilesToRemove = new List<Projectile>();
        }

        public static void Initialize(Game game)
        {
            if (game == null) return;

            bulletManager = new ProjectileManager(game);
            game.Components.Add(bulletManager);
        }

        protected override void LoadContent()
        {
            texture = Game.Content.Load<Texture2D>(@"Graphics/Bullets");
            base.LoadContent();
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.Begin();
            foreach (Bullet currBullet in projectiles)
                currBullet.Draw(spriteBatch);
            spriteBatch.End();
        }

        public override void Update(GameTime gameTime)
        {
            foreach (Projectile currProjectile in projectiles)
            {
                currProjectile.Update(gameTime, this.Game.GraphicsDevice.Viewport.Bounds);
                if (currProjectile.IsOutOfClientBounds(this.Game.GraphicsDevice.Viewport.Bounds)) projectilesToRemove.Add(currProjectile);
            }

            foreach (Bullet remBullet in projectilesToRemove)
                projectiles.Remove(remBullet);

            projectilesToRemove.Clear();
        }


        public static void Add(Projectile projectile)
        {
            projectiles.Add(projectile);
        }
    }
}
