﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tanked.Enemy
{
    public enum EnemyType
    {
        Level1Tank,
        Level2Tank,
        Level3Tank,
        Level4Tank,
    }

    public sealed class EnemyFactory
    {
        private EnemyFactory()
        {
        }

        public Enemy CreateEnemy(EnemyType Type, Vector2 Position)
        {
            Enemy newEnemy = null;
            switch (Type)
            {
                case EnemyType.Level1Tank:
                case EnemyType.Level2Tank:
                case EnemyType.Level3Tank:
                case EnemyType.Level4Tank:
                    newEnemy = new Enemy();
                    break;
                default:
                    throw new Exception("The enemy type " + Type + " is not recognized");
            }
            return newEnemy;
        }

        static readonly EnemyFactory _instance = new EnemyFactory();

        public static EnemyFactory Instance
        {
            get { return _instance; }
        }
    }
}
