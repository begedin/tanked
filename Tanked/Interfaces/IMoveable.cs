﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Tanked.Timing;

namespace Tanked.Interfaces
{
    interface IMoveable
    {
        float Speed { get; set; }
    }
}
