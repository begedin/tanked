﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Tanked.Interfaces
{
    interface ICamera2D
    {
        Vector2 Position { get; }
    }
}
