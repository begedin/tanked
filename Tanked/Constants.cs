﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tanked
{
    static class Constants
    {
        public const int TileSize = 48;
        public const int GraphicSize = 24;
        public const float DefaultScale = (float)TileSize / GraphicSize;
    }
}
