﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input.Touch;
using System.IO;
using Tanked.ScreenManagement;

namespace Tanked.GameScreens
{
    public enum ScreenState
    {
        TransitionOn,
        Active,
        TransitionOff,
        Hidden,
    }

    /// <summary>
    /// A screen is a single layer that has update and draw logic.
    /// It can be combined with other layers to build up a complex menu
    /// system.
    /// 
    /// The main menu or the options menu are both screens. The gameplay screen 
    /// is also a game screen, albeit with much more logic behind it.
    /// </summary>
    public abstract class GameScreen
    {
        #region Properties & Fields

        bool isPopup = false;
        /// <summary>
        /// Normally, the active game screen covers up all the other screens.
        /// If IsPopup is true, then the screen only partially covers the screens bellow.
        /// 
        /// TODO: Split PopupScreen into separate class.
        /// </summary>
        public bool IsPopup
        {
            get { return isPopup; }
            protected set { isPopup = value; }
        }

        TimeSpan transitionOnTime = TimeSpan.Zero;
        /// <summary>
        /// The screens appear gradually instead of instantly, to keep things more juicy
        /// </summary>
        public TimeSpan TransitionOnTime
        {
            get { return transitionOnTime; }
            protected set { transitionOnTime = value; }
        }

        TimeSpan transitionOffTime = TimeSpan.Zero;
        /// <summary>
        /// The screens disappear gradually instead of instantly, to keep things more juicy
        /// </summary>
        public TimeSpan TransitionOffTime
        {
            get { return transitionOffTime; }
            protected set { transitionOffTime = value; }
        }

        float transitionPosition = 1;
        /// <summary>
        /// The current position of the screen transition process.
        /// 0 = fully active, 1 = fully transitioned ot nothing.
        /// </summary>
        public float TransitionPosition
        {
            get { return transitionPosition; }
            protected set { transitionPosition = value; }
        }
        /// <summary>
        /// The current alpha of the screen transition.
        /// 1 - fully visible, 0 - fully invisible
        /// </summary>
        public float TransitionAlpha
        {
            get { return 1f - TransitionPosition; }
        }


        ScreenState screenState = ScreenState.TransitionOn;
        /// <summary>
        /// The current transition state.
        /// </summary>
        public ScreenState ScreenState
        {
            get { return screenState; }
            protected set { screenState = value; }
        }

        bool isExiting = false;
        /// <summary>
        /// When the screen is transitioning off, it's either temporarily going away
        /// (isExiting = false), or it's leaving for good (isExiting = true)
        /// </summary>
        public bool IsExiting
        {
            get { return isExiting; }
            protected internal set { isExiting = value; }
        }

        bool otherScreenHasFocus;
        /// <summary>
        /// Checks if this screen is active and can respond to user input.
        /// </summary>
        public bool IsActive
        {
            get
            {
                return !otherScreenHasFocus && (ScreenState == ScreenState.TransitionOn
                    || screenState == ScreenState.Active);
            }
        }

        ScreenManager screenManager;
        /// <summary>
        /// Gets the manager that this screen belongs to
        /// </summary>
        public ScreenManager ScreenManager
        {
            get { return screenManager; }  
            internal set { screenManager = value; }
        }

        PlayerIndex? controllingPlayer;
        /// <summary>
        /// Gets the index of the currently controlling player. For now, this can be
        /// null to accept input from any player. In the future, we might have multiplayer.
        /// </summary>
        public PlayerIndex? ControllingPlayer
        {
            get { return controllingPlayer; }
            internal set { controllingPlayer = value; }
        }
        /// <summary>
        /// Gets the gestures the screen is interested in. We should alwas excplicitely define
        /// supported gestures to increase controll precision.
        /// </summary>
        public GestureType EnabledGestures
        {
            get { return enabledGestures; }
            protected set
            {
                enabledGestures = value;
                //the scrren manager handles everything during screen changes, but if 
                //this screen is active and supported gestures are changing, we need to 
                //handle it ourselves
                if (ScreenState == ScreenState.Active)
                    TouchPanel.EnabledGestures = value;
            }
        }
        GestureType enabledGestures = GestureType.None;

        #endregion

        #region Initialization

        /// <summary>
        /// Loads content for the screen
        /// </summary>
        public virtual void LoadContent() { }
        /// <summary>
        /// Unloads screen content
        /// </summary>
        public virtual void UnloadContent() { }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Allows the screen to run logic, such as updating the transition position.
        /// Unlike HandleInput, this method is called regardless of whether the screen is
        /// active, hidden, or in the middle of transition.
        /// </summary>
        public virtual void Update(GameTime gameTime, bool otherScreenHasFocus,
                                                      bool coveredByOtherScreen)
        {
            this.otherScreenHasFocus = otherScreenHasFocus;

            if (isExiting)
            {
                //If the screen going away to day, it needs to transition off
                screenState = ScreenState.TransitionOff;

                if (!UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    //when the transition is finished, the screen needs to be removed
                    ScreenManager.RemoveScreen(this);
                }
            }
            else if (coveredByOtherScreen)
            {
                //If the screen is covered by another, it should transition off
                if (UpdateTransition(gameTime, transitionOffTime, 1))
                {
                    //still busy transitioning
                    screenState = ScreenState.TransitionOff;
                }
                else
                {
                    //Transition finished
                    screenState = ScreenState.Hidden;
                }
            }
            else
            {
                //Otherwise, the screen should transition on and become active
                if (UpdateTransition(gameTime, transitionOnTime, -1))
                {
                    //still busy transitionin
                    screenState = ScreenState.TransitionOn;
                }
                else
                {
                    //Transition finished
                    screenState = ScreenState.Active;
                }
            }
        }


        /// <summary>
        /// Helper for updating the screen transition position
        /// </summary>
        bool UpdateTransition(GameTime gameTime, TimeSpan time, int direction)
        {
            float transitionDelta;

            //How much should we move by
            if (time == TimeSpan.Zero)
                transitionDelta = 1;

            else
                transitionDelta = (float)(gameTime.ElapsedGameTime.TotalMilliseconds /
                                                    time.TotalMilliseconds);

            //update the transition position
            transitionPosition += transitionDelta * direction;

            //did we reach the end of the transition?
            if (((direction < 0) && (transitionPosition <= 0)) ||
            ((direction > 0) && (transitionPosition >= 1)))
            {
                transitionPosition = MathHelper.Clamp(transitionPosition, 0, 1);
                return false;
            }

            //otherwise we are still busy transitioning
            return true;
        }

        /// <summary>
        /// Allows the screen to handle user input. This method is only
        /// called when the scren is active and in focuse, unlike Update
        /// </summary>
        public virtual void HandleInput(InputState input) { }

        /// <summary>
        /// This is called when the screen should draw itself
        /// </summary>
        public virtual void Draw(GameTime gameTime) { }
        
        #endregion

        #region Public Methods

        public void ExitScreen()
        {
            if (TransitionOffTime == TimeSpan.Zero)
            {
                //if the screen has a zero transition time, it needs to be removed
                //immediately
                ScreenManager.RemoveScreen(this);
            }
            else
            {
                //otherwise, just flag it as exiting and let it take care of itself
                isExiting = true;
            }
        }

        #endregion
    }
}
