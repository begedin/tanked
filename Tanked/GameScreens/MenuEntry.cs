﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.ScreenManagement;

namespace Tanked.GameScreens
{
    /// <summary>
    /// Represents a single entry in a MenuScreen. By default, it just draws a text
    /// string, but it can be inherited to display in different ways.
    /// </summary>
    class MenuEntry
    {
        #region Fields

        string text;
        Vector2 position;

        /// <summary>
        /// Tracks a fading selection effect on the entry
        /// </summary>
        /// <remarks>
        /// The entries transition out of the selection effect
        /// when they are deselected
        /// </remarks>
        float selectionFade;

        #endregion

        #region Properties

        /// <summary>
        /// The text content of the menu entry
        /// </summary>
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        /// <summary>
        /// the position at which the entry is drawn. This is set by the MenuScreen 
        /// each frame in the update.
        /// </summary>
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Constructs a new menu entry
        /// </summary>
        /// <param name="text">The text the entry will display.</param>
        public MenuEntry(string text)
        {
            this.text = text;
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Updates the menu entry
        /// </summary>
        public virtual void Update(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            //There is no such thing as a selected item on a touch device, so 
            //isSelected will always be false
#if TOUCH
            isSelected = false;
#endif

            //when the menu selection chages, entries gradually fade between their
            //selected and deselected appearance
            float fadeSpeed = (float)gameTime.ElapsedGameTime.TotalSeconds * 4;

            if (isSelected)
                selectionFade = Math.Min(selectionFade + fadeSpeed, 1);
            else
                selectionFade = Math.Max(selectionFade - fadeSpeed, 0);

        }

        /// <summary>
        /// Draws the menu entry. This can be overriden to customize how it displays
        /// </summary>
        public virtual void Draw(MenuScreen screen, bool isSelected, GameTime gameTime)
        {
            //Draw the selected entry in yellow, otherwise white
            Color color = isSelected ? Color.Yellow : Color.White;

            //pulsate the size of the selected menu entry
            double time = gameTime.TotalGameTime.TotalSeconds;

            float pulsate = (float)Math.Sin(time * 6) + 1;
            float scale = 1 + pulsate * 0.05f * selectionFade;

            //modify the color with the screen alpha
            color *= screen.TransitionAlpha;

            ScreenManager screenManager = screen.ScreenManager;
            SpriteBatch spriteBatch = screenManager.SpriteBatch;
            SpriteFont font = screenManager.Font;

            Vector2 origin = new Vector2(0, font.LineSpacing / 2);

            spriteBatch.DrawString(font, text, position, color, 0,
                origin, scale, SpriteEffects.None, 0);
        }

        /// <summary>
        /// Returns how much vertical space the entry requires
        /// </summary>
        public virtual int GetHeight(MenuScreen screen)
        {
            return screen.ScreenManager.Font.LineSpacing;
        }

        /// <summary>
        /// Returns how much horizontal space the entry requires
        /// </summary>
        public virtual int GetWidth(MenuScreen screen)
        {
            return (int)screen.ScreenManager.Font.MeasureString(Text).X;
        }

        #endregion

        #region Events


        /// <summary>
        /// Event raised when the menu entry is selected.
        /// </summary>
        public event EventHandler<PlayerIndexEventArgs> Selected;


        /// <summary>
        /// Method for raising the Selected event.
        /// </summary>
        protected internal virtual void OnSelectEntry(PlayerIndex playerIndex)
        {
            if (Selected != null)
                Selected(this, new PlayerIndexEventArgs(playerIndex));
        }


        #endregion
    }
}
