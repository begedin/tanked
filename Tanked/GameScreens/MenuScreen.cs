﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.ScreenManagement;

namespace Tanked.GameScreens
{
    /// <summary>
    /// Base class for screens which contain a menu of options (menu entries=-
    /// The user can move up or down through entries, or cancel to go to the previous screen
    /// </summary>
    abstract class MenuScreen : GameScreen
    {
        #region Fields

        List<MenuEntry> menuEntries = new List<MenuEntry>();
        int selectedEntry = 0;
        string menuTitle;

        #endregion

        #region Properties

        /// <summary>
        /// Gets the list of meni entries, so derived classes can
        /// add or change the menu contents
        /// </summary>
        protected IList<MenuEntry> MenuEntries
        {
            get { return menuEntries; }
        }

        #endregion

        #region Initialization

        public MenuScreen(string menuTitle)
        {
            this.menuTitle = menuTitle;

            TransitionOnTime = TimeSpan.FromSeconds(0.5);
            TransitionOffTime = TimeSpan.FromSeconds(0.5);
        }

        #endregion

        #region HandleInput

        /// <summary>
        /// Reads the users input. Changes selected entries and advances/goes back.
        /// </summary>
        public override void HandleInput(InputState input)
        {
            //Move to previous entry
            if (input.IsMenuUp(ControllingPlayer))
            {
                selectedEntry--;

                if (selectedEntry < 0)
                    selectedEntry = menuEntries.Count - 1;
            }

            //Move to next entry
            if (input.IsMenuDown(ControllingPlayer))
            {
                selectedEntry++;

                if (selectedEntry >= menuEntries.Count)
                    selectedEntry = 0;
            }

            //Accept or cancel the menu. If the controlling player is null, the input state returns to us which
            //player provided the input. We pass this to the OnSelectEntry and OnCancel, so
            //they can tell which player did it.
            PlayerIndex playerIndex;

            if (input.IsMenuSelect(ControllingPlayer, out playerIndex))
            {
                OnSelectEntry(selectedEntry, playerIndex);
            }
            else if (input.IsMenuCancel(ControllingPlayer, out playerIndex))
            {
                OnCancel(playerIndex);
            }
        }

        protected virtual void OnCancel(PlayerIndex playerIndex)
        {
            ExitScreen();
        }

        protected void OnCancel(object sender, PlayerIndexEventArgs e)
        {
            OnCancel(e.PlayerIndex);
        }

        protected virtual void OnSelectEntry(int selectedEntry, PlayerIndex playerIndex)
        {
            menuEntries[selectedEntry].OnSelectEntry(playerIndex);
        }


        #endregion

        #region Update and Draw

        /// <summary>
        /// Allows the screen to change the position of the menu entries.
        /// By default, all entries are in a vertical list, center on the screen.
        /// </summary>
        protected virtual void UpdateMenuEntryLocations()
        {
            ///menu slides into place on a power curve
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            //start at Y = 175, each X value is generated per entry
            //TODO: Change this as you please
            Vector2 position = new Vector2(0f, 175f);

            //update each menu entry's location
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];

                //center each entry horizontaly
                position.X = ScreenManager.GraphicsDevice.Viewport.Width / 2
                     - menuEntry.GetWidth(this) / 2;

                if (ScreenState == ScreenState.TransitionOn)
                    position.X -= transitionOffset * 256;
                else
                    position.X += transitionOffset * 512;

                //set the position
                menuEntry.Position = position;

                //move down to next entry
                position.Y += menuEntry.GetHeight(this);
            }
        }

        public override void Update(GameTime gameTime, bool otherScreenHasFocus, bool coveredByOtherScreen)
        {
            base.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

            for (int i = 0; i < menuEntries.Count; i++)
            {
                bool isSelected = IsActive && (i == selectedEntry);
                menuEntries[i].Update(this, isSelected, gameTime);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            //sort out the entries
            UpdateMenuEntryLocations();

            GraphicsDevice graphics = ScreenManager.GraphicsDevice;
            SpriteBatch spriteBatch = ScreenManager.SpriteBatch;
            SpriteFont font = ScreenManager.Font;

            spriteBatch.Begin();

            //draw each entry
            for (int i = 0; i < menuEntries.Count; i++)
            {
                MenuEntry menuEntry = menuEntries[i];
                bool isSelected = IsActive && (i == selectedEntry);

                menuEntry.Draw(this, isSelected, gameTime);
            }


            ///make the menu slide into place during transitions
            float transitionOffset = (float)Math.Pow(TransitionPosition, 2);

            //draw the menu title
            Vector2 titlePosition = new Vector2(graphics.Viewport.Width/2, 80);
            Vector2 titleOrigin = font.MeasureString(menuTitle) /2;
            Color titleColor = new Color(192, 192, 192) * TransitionAlpha;
            float titleScale = 1.25f;

            titlePosition.Y -= transitionOffset * 100;

            spriteBatch.DrawString(font, menuTitle, titlePosition, titleColor, 0,
                titleOrigin, titleScale, SpriteEffects.None, 0);

            spriteBatch.End();
        }

        #endregion
    }
}
