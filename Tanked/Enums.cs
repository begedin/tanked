﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tanked
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right,
    }

    public enum Owner
    {
        Player,
        Enemy
    }
}
