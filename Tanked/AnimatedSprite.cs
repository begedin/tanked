﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Tanked.Interfaces;
using Tanked.Timing;

namespace Tanked
{
    /// <summary>
    /// The low level object for most visible animated entities on the screen
    /// 
    /// CREDIT TO ORIGINAL https://github.com/crneville/XNA/blob/master/1061fight
    /// 
    /// MODIFED: Sprite can update it's frame and has a position, that's it.
    /// </summary>
    public abstract class AnimatedSprite
    {

        #region Identifiers
        static public int SpriteUID = 0;
        protected int spriteUID = 0;

        /// <summary>
        /// Generates and returns a unique id for the next sprite
        /// </summary>
        static public int GetSpriteUID
        {
            get { return AnimatedSprite.SpriteUID++; }
        }

        /// <summary>
        /// The unique ID for this sprite
        /// </summary>
        public int SpriteIdentifier
        {
            get { return spriteUID; }
        }
        #endregion

        #region Fields

        //Constants
        const float defaultScale = 1.0f;
        const int defaultMilisecondsPerFrame = 32;

        //Graphics
        protected Texture2D spriteTexture;
        protected float spriteScale = 1.0f;

        //Coordinates
        protected Vector2 currentPosition;
        protected float angle;
        protected Vector2 origin;

        //Spritesheet info
        protected bool verticalSheetAxisEnabled = true;
        protected Point spriteSheetFrameSize;
        protected Point spriteSheetStartingFrame;
        protected Point spriteSheetCurrentFrame;
        protected Point spriteSheetDimensions;

        //frame timing
        Timer frameTimer;

        //FPS control
        int milisecondsPerFrame = defaultMilisecondsPerFrame;

        //Collision info
        protected Vector4 spriteCollisionBoxOffset;

        #endregion

        #region Properties

        public float Width
        {
            get { return spriteSheetFrameSize.X * spriteScale; }
        }

        public float Height
        {
            get { return spriteSheetFrameSize.Y * spriteScale; }
        }

        public int FPS
        {
            get { return milisecondsPerFrame; }
            set { milisecondsPerFrame = value; }
        }

        public Point CurrentFrame
        {
            get { return spriteSheetCurrentFrame; }
        }

        public Point SpriteFrameSize
        {
            get { return spriteSheetFrameSize; }
        }

        public Vector2 Position
        {
            get { return currentPosition; }
            set { currentPosition = value; }
        }

        /// <summary>
        /// The computed collision rectangle for the sprite
        /// </summary>
        public Rectangle CollisionRectangle
        {
            get
            {
                return new Rectangle(
                    (int)(currentPosition.X + spriteCollisionBoxOffset.X),
                    (int)(currentPosition.Y + spriteCollisionBoxOffset.Y),
                    (int)((spriteSheetFrameSize.X - spriteCollisionBoxOffset.X - spriteCollisionBoxOffset.Z) * spriteScale),
                    (int)((spriteSheetFrameSize.Y - spriteCollisionBoxOffset.Y - spriteCollisionBoxOffset.W) * spriteScale));
            }
        }

        /// <summary>
        /// The NW corner of the collision rectangle
        /// </summary>
        public Vector2 NWCorner
        {
            get
            {
                float x = 0.0f;
                float y = 0.0f;
                x = currentPosition.X + spriteCollisionBoxOffset.X;
                y = currentPosition.Y + spriteCollisionBoxOffset.Y;
                return new Vector2(x, y);
            }
        }

        /// <summary>
        /// The SW corner of the collision rectangle
        /// </summary>
        public Vector2 SWCorner
        {
            get
            {
                float x = 0.0f;
                float y = 0.0f;
                x = currentPosition.X + spriteCollisionBoxOffset.X;
                y = currentPosition.Y + (spriteSheetFrameSize.Y - spriteCollisionBoxOffset.Y - spriteCollisionBoxOffset.W) * spriteScale;
                return new Vector2(x, y);
            }
        }

        /// <summary>
        /// The NE corner of the collision rectangle
        /// </summary>
        public Vector2 NECorner
        {
            get
            {
                float x = 0.0f;
                float y = 0.0f;
                x = currentPosition.X + (spriteSheetFrameSize.X - spriteCollisionBoxOffset.X - spriteCollisionBoxOffset.Z) * spriteScale;
                y = currentPosition.Y + spriteCollisionBoxOffset.Y;
                return new Vector2(x, y);
            }
        }

        /// <summary>
        /// The SE corner of the collision rectangle
        /// </summary>
        public Vector2 SECorner
        {
            get
            {
                float x = 0.0f;
                float y = 0.0f;
                x = currentPosition.X + (spriteSheetFrameSize.X - spriteCollisionBoxOffset.X - spriteCollisionBoxOffset.Z) * spriteScale;
                y = currentPosition.Y + (spriteSheetFrameSize.Y - spriteCollisionBoxOffset.Y - spriteCollisionBoxOffset.W) * spriteScale;
                return new Vector2(x, y);
            }
        }

        /// <summary>
        /// The Y coordinate of the top of the collision rectangle
        /// </summary>
        public float Top
        {
            get
            {
                return currentPosition.Y;
            }
        }

        /// <summary>
        /// The Y coordinate of the bottom of the collision rectangle
        /// </summary>
        public float Bottom
        {
            get
            {
                return currentPosition.Y + (spriteSheetFrameSize.Y - spriteCollisionBoxOffset.Y - spriteCollisionBoxOffset.W) * spriteScale;
            }
        }

        /// <summary>
        /// The X coordinate of the right side of the collision rectangle
        /// </summary>
        public float Right
        {
            get
            {
                return currentPosition.X + (spriteSheetFrameSize.X - spriteCollisionBoxOffset.X - spriteCollisionBoxOffset.Z) * spriteScale;
            }
        }

        /// <summary>
        /// The X coordinate of the left side of the collision rectangle
        /// </summary>
        public float Left
        {
            get
            {
                return currentPosition.Y;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// New sprite with specified texture and frame size. The rest can be set later
        /// </summary>
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize)
            : this(spriteTexture, spriteFrameSize, Vector4.Zero, new Point(0, 0), new Point(0, 0), Vector2.Zero, 0, defaultScale)
        {
        }

        /// <summary>
        /// New Parameters: Collision Offsets
        /// </summary>
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize, Vector4 spriteCollisionBoxOffset)
            : this(spriteTexture, spriteFrameSize, spriteCollisionBoxOffset, new Point(0, 0), new Point(0, 0), Vector2.Zero, 0, defaultScale)
        {
        }

        /// <summary>
        /// New Parameters: Sprite Sheet (Multiple Images/ Animated sprite)
        /// </summary>
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize, Vector4 spriteCollisionBoxOffset, Point spriteStartingFrame, Point spriteSheetDimensions)
            : this(spriteTexture, spriteFrameSize, spriteCollisionBoxOffset, spriteStartingFrame, spriteSheetDimensions, Vector2.Zero, 0, defaultScale)
        {
        }

        /// <summary>
        /// New Parameters: Location
        /// </summary>
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize, Vector4 spriteCollisionBoxOffset, Point spriteStartingFrame, Point spriteSheetDimensions, Vector2 startingPosition)
            : this(spriteTexture, spriteFrameSize, spriteCollisionBoxOffset, spriteStartingFrame, spriteSheetDimensions, startingPosition, 0, defaultScale)
        {
        }

        /// <summary>
        /// New Parameter: Angle
        /// </summary>
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize, Vector4 spriteCollisionBoxOffset, Point spriteStartingFrame, Point spriteSheetDimensions, Vector2 startingPosition, float startingAngle)
            : this(spriteTexture, spriteFrameSize, spriteCollisionBoxOffset, spriteStartingFrame, spriteSheetDimensions, startingPosition, 0, defaultScale)
        {
        }

        // New Parameters: Sprite Scaling
        public AnimatedSprite(Texture2D spriteTexture, Point spriteFrameSize, Vector4 spriteCollisionBoxOffset, Point spriteStartingFrame, Point spriteSheetDimensions, Vector2 startingPosition, float startingAngle, float spriteScale)
        {
            this.spriteUID = AnimatedSprite.GetSpriteUID;

            this.spriteTexture = spriteTexture;
            this.spriteSheetFrameSize = spriteFrameSize;
            this.spriteCollisionBoxOffset = spriteCollisionBoxOffset;
            this.spriteSheetStartingFrame = spriteStartingFrame;
            this.spriteSheetCurrentFrame = spriteStartingFrame;
            this.spriteSheetDimensions = spriteSheetDimensions;
            this.currentPosition = startingPosition;
            this.spriteScale = spriteScale;

            this.origin = new Vector2(spriteFrameSize.X / 2, spriteFrameSize.Y / 2);

            this.frameTimer = new Timer("animationTimer", milisecondsPerFrame);
            this.frameTimer.Start();
        }

        #endregion

        #region Member Functions

        /// <summary>
        /// Updates the current frame of the sprite only. 
        /// For motion, this function needs to be overriden and inherited
        /// </summary>
        public virtual void Update(GameTime gameTime, Rectangle clientBounds)
        {
            if (frameTimer.IsExpired)
            {
                ++spriteSheetCurrentFrame.X;

                if (spriteSheetCurrentFrame.X >= spriteSheetDimensions.X)
                {
                    spriteSheetCurrentFrame.X = spriteSheetStartingFrame.X;

                    if (verticalSheetAxisEnabled)
                    {
                        ++spriteSheetCurrentFrame.Y;
                        if (spriteSheetCurrentFrame.Y >= spriteSheetDimensions.Y)
                        {
                            spriteSheetCurrentFrame.Y = spriteSheetStartingFrame.Y;
                        }
                    }
                }

                frameTimer.Reset();
                frameTimer.Start();
            }
        }

        /// <summary>
        /// Draws the sprite with respect to the current parameters
        /// </summary>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(
                spriteTexture,
                currentPosition,
                new Rectangle(
                    (int)(spriteSheetStartingFrame.X + spriteSheetCurrentFrame.X * spriteSheetFrameSize.X),
                    (int)(spriteSheetStartingFrame.X + spriteSheetCurrentFrame.Y * spriteSheetFrameSize.Y),
                    (int)(spriteSheetFrameSize.X),
                    (int)(spriteSheetFrameSize.Y)),
                Color.White,
                angle, origin,
                spriteScale,
                SpriteEffects.None, 0);
        }

        /// <summary>
        /// Returns true if the sprite is out client bounds
        /// </summary>
        public virtual bool IsOutOfClientBounds(Rectangle clientBounds)
        {
            if (
                currentPosition.X < -spriteSheetFrameSize.X ||
                currentPosition.X > clientBounds.Width ||
                currentPosition.Y < -spriteSheetFrameSize.Y ||
                currentPosition.Y > clientBounds.Height)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets the sprite scale to the new value
        /// </summary>
        /// <param name="spriteScale"></param>
        public void ModifyScale(float spriteScale)
        {
            this.spriteScale = spriteScale;
        }

        /// <summary>
        /// Resets the sprite scale to the default value
        /// </summary>
        public void ResetScale()
        {
            spriteScale = defaultScale;
        }

        #endregion
    }
}
