﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tanked.Projectiles;

namespace Tanked.Weapon
{
    abstract class Weapon
    {
        protected enum State
        {
            Firing, Idle,
        }

        protected State state;

        public abstract void Fire();
    }
}
