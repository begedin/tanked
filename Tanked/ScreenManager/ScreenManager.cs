﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Tanked.GameScreens;

namespace Tanked.ScreenManagement
{
    /// <summary>
    /// A screen manager is a component which manages one or more GameScreen instances
    /// in the form of a stack. It calls proper Update and Draw methods and routes input to 
    /// proper screens in the stack.
    /// </summary>
    public class ScreenManager : DrawableGameComponent
    {
        #region Fields

        List<GameScreen> screens = new List<GameScreen>();
        List<GameScreen> screensToUpdate = new List<GameScreen>();

        InputState input = new InputState();

        SpriteBatch spriteBatch;
        SpriteFont font;
        Texture2D blankTexture;

        bool isInitialized;
        bool traceEnabled;

        #endregion

        #region Properties

        /// <summary>
        /// A default SpriteBatch shared by all the screens. This saves
        /// each screen from having to bother creating their own instance.
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get { return spriteBatch; }
        }

        /// <summary>
        /// A default font shared by all the screens. This saves each screen 
        /// from having to load their own local copy.
        /// </summary>
        public SpriteFont Font
        {
            get { return font; }
        }
        /// <summary>
        /// If this is true, the manager prints out a list of all the screens each time
        /// it updates. Used for debugging
        /// </summary>
        public bool TraceEnabled
        {
            get { return traceEnabled; }
            set { traceEnabled = value; }
        }

        #endregion

        #region Initialization

        /// <summary>
        /// Constructs a new screen manager component.
        /// </summary>
        public ScreenManager(Game game)
            : base(game)
        {
            //Enabled gestures need to be set in order to be queried, but for now
            //we don't indent to use them
            TouchPanel.EnabledGestures = GestureType.None;
        }

        /// <summary>
        /// Initializes the screen manager component.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();

            isInitialized = true;
        }

        /// <summary>
        /// loads the assets
        /// </summary>
        protected override void LoadContent()
        {
            //load screen manager content
            ContentManager content = Game.Content;

            spriteBatch = new SpriteBatch(GraphicsDevice);
            font = content.Load<SpriteFont>(@"Fonts/Calibri14");
            blankTexture = content.Load<Texture2D>(@"Graphics/blank");

            //load all the screens
            foreach (GameScreen screen in screens)
                screen.LoadContent();
        }

        /// <summary>
        /// unloads the screens
        /// </summary>
        protected override void UnloadContent()
        {
            foreach (GameScreen screen in screens)
                screen.UnloadContent();  
        }

        #endregion

        #region Update and Draw

        /// <summary>
        /// Allows each screen to run logic. All screens are updated at every tick
        /// </summary>
        public override void Update(GameTime gameTime)
        {
            //read input
            input.Update();
            //make a copy of the master screen list, to avoid issues with adding/removing
            screensToUpdate.Clear();
            foreach (GameScreen screen in screens)
                screensToUpdate.Add(screen);

            bool otherScreenHasFocus = !Game.IsActive;
            bool coveredByOtherScreen = false;

            //loop while there are screens to be updated
            while (screensToUpdate.Count > 0)
            {
                //Pop the topmost screen
                GameScreen screen = screensToUpdate[screensToUpdate.Count - 1];
                screensToUpdate.RemoveAt(screensToUpdate.Count - 1);

                //Update the screen
                screen.Update(gameTime, otherScreenHasFocus, coveredByOtherScreen);

                //if this is the first active screen, it needs to handle input
                if (screen.ScreenState == ScreenState.TransitionOn ||
                    screen.ScreenState == ScreenState.Active)
                {
                    if (!otherScreenHasFocus)
                    {
                        screen.HandleInput(input);
                        //we handled the active screen's input. don't handle input
                        //for any more screens
                        otherScreenHasFocus = true;
                    }
                }

                //if the active screen is not a popup, inform subsequent screens that they
                //are covered by it
                if (!screen.IsPopup)
                    coveredByOtherScreen = true;
            }

            if (traceEnabled) TraceScreens();
        }

        /// <summary>
        /// Prints out a list of all the screens to the Debug console
        /// </summary>
        void TraceScreens()
        {
            List<string> screenNames = new List<string>();
            foreach (GameScreen screen in screens)
                screenNames.Add(screen.GetType().Name);

            Debug.WriteLine(string.Join(", ", screenNames.ToArray()));
        }

        //Draws all non-hidden screens
        public override void Draw(GameTime gameTime)
        {
            foreach (GameScreen screen in screens)
            {
                if (screen.ScreenState == ScreenState.Hidden)
                    continue;

                screen.Draw(gameTime);
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Adds a new screen to the screen manager
        /// </summary>
        /// <param name="screen">The screen to be added</param>
        /// <param name="controllingPlayer">The controlling player. Null for any.</param>
        public void AddScreen(GameScreen screen, PlayerIndex? controllingPlayer)
        {
            screen.ControllingPlayer = controllingPlayer;
            screen.ScreenManager = this;
            screen.IsExiting = false;

            //if we already have a graphics device, tell the screen to load content
            if (isInitialized)
            {
                screen.LoadContent();
            }

            screens.Add(screen);

            //update the touchpanel to respond to gestures the screen is interested in
            TouchPanel.EnabledGestures = screen.EnabledGestures;
        }

        /// <summary>
        /// Removes a screen from the screen manager. We normally use ExitScreen to 
        /// gradually transition off, but there are exceptions when this is used instead. 
        /// </summary>
        /// <param name="gameScreen">The screen to be instantly removed</param>
        public void RemoveScreen(GameScreen screen)
        {
            //if there was loading, unload
            if (isInitialized)
            {
                screen.UnloadContent();
            }

            screens.Remove(screen);
            screensToUpdate.Remove(screen);

            //if there are still screens, set enabled gestures to support the topmost screen
            if (screens.Count > 0)
            {
                TouchPanel.EnabledGestures = screens[screens.Count - 1].EnabledGestures;
            }
        }

        /// <summary>
        /// Expose an array of all the screens. A copy is returned since we are only
        /// allowed to add and remove screens via the AddScreen/RemoveScreen methods.
        /// </summary>
        public GameScreen[] GetScreens()
        {
            return screens.ToArray();
        }

        /// <summary>
        /// Helper which draws a translucent black fullscreen sprite, used for fading screens
        /// in and out, and for darkening background behind popups.
        /// </summary>
        /// <param name="alpha"></param>
        public void FadeBackBufferToBlack(float alpha)
        {
            Viewport viewport = GraphicsDevice.Viewport;

            spriteBatch.Begin();

            spriteBatch.Draw(blankTexture,
                            new Rectangle(0, 0, viewport.Width, viewport.Height),
                            Color.Black * alpha);

            spriteBatch.End();
        }

        #endregion
    }
}
